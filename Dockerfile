FROM python:3

RUN apt update && apt install -y \ 
	sqlite3 \
    libsasl2-dev \
    libldap2-dev \
    default-mysql-client \
    default-libmysqlclient-dev \
    python-ldap \
    postgresql-client \
    && apt clean

COPY pip-reqs.txt pip-reqs.txt
RUN pip install -r pip-reqs.txt && pip install uwsgi mysqlclient psycopg2

COPY . /app/ 

WORKDIR /app/

RUN SECRET_KEY=fake python manage.py collectstatic --noinput

CMD python manage.py migrate --noinput && \
    uwsgi --http 0.0.0.0:8080 --static-map /static=/app/static --master --processes=5 --harakiri=20 --max-requests=5000 --env DJANGO_SETTINGS_MODULE=robopoly.settings --module=robopoly.wsgi:application --log-master --log-format '%(addr) - %(user) [%(ltime)] "%(method) %(uri) %(proto)" %(status) %(size) "%(referer)" "%(uagent)"' 2>/dev/null
